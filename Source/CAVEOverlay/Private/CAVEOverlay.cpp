// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "CAVEOverlay.h"
#include "CAVEOverlaySettings.h"
#include "CAVEOverlayController.h"
#include "Kismet/GameplayStatics.h"

#define LOCTEXT_NAMESPACE "FCAVEOverlayModule"

void FCAVEOverlayModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module

	On_Post_World_Initialization_Delegate.BindRaw(this, &FCAVEOverlayModule::OnSessionStart);
	FWorldDelegates::OnPostWorldInitialization.Add(On_Post_World_Initialization_Delegate);
}

void FCAVEOverlayModule::OnSessionStart(UWorld* World, const UWorld::InitializationValues)
{
	if (!World->IsGameWorld())
		return;

	const UCAVEOverlaySettings* Settings = GetDefault<UCAVEOverlaySettings>();

	//Test if already in
	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsOfClass(World, ACAVEOverlayController::StaticClass(), Actors);

	if((Settings->DefaultActivationType == DefaultActivationType_ON
		!= Settings->excludedMaps.ContainsByPredicate(
			[World](const FSoftObjectPath& Map)	{return Map.GetAssetName() == World->GetName();}
		)) && Actors.Num() == 0){
		World->SpawnActor(ACAVEOverlayController::StaticClass());
	}
}


void FCAVEOverlayModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FCAVEOverlayModule, CAVEOverlay)
